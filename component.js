class ControlPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      context: props.context,
      plot: new GlobalView(document.getElementById('divGlobalView')),
      hasData: false,
      pointSize: 1
    };
    this.onClick = this.onClick.bind(this);
    this.changeSize = this.changeSize.bind(this);
  }

  onClick() {
    new RandomDataset(1000, 3, (dataset) => { this.state.plot.load(dataset, 0, 1, 2, 2); });
    this.state.hasData = true;
  }

  changeSize = (event) => {
    this.state.pointSize = event.target.value;
    console.log(this.state.pointSize);
    this.state.plot.setOption("pointSize", this.state.pointSize);
  }

  render() {
    return (
      <div className='alert alert-success' role='alert'>
        <h3>Hello, from React!</h3>
        <button type='button' className='btn btn-default' onClick={this.onClick}>Click Me</button>
        <input id="rPointSize" className="option" type="range" min="1" max="100" defaultValue="10" step="1" onChange={this.changeSize}></input>
      </div>
    );
  }
}